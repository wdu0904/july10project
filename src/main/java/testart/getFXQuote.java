package testart;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;

import org.apache.commons.io.IOUtils;
public class getFXQuote
{
    public String filepath;
    public static HashMap<String, Double> cache = new HashMap<String, Double>();
    public getFXQuote(String Filepath){
        filepath = Filepath;
        BufferedReader br;


  
        try
        {
            //String xml = IOUtils.toString(
            //  this.getClass ().getResourceAsStream ("rate.csv"),"UTF-8");
            String basePath = new File("rates.csv").getAbsolutePath();
            System.out.print (basePath);
            String path = "./";
            br = new BufferedReader(new FileReader(basePath));
            String line= br.readLine ();
            line =br.readLine ();
            while(line !=null && !line.isEmpty())
            {
                String[] countries = line.split (",");
                //System.out.println (countries[0]+countries[1]+countries[2]);
                cache.put (countries[0]+countries[1], Double.parseDouble (countries[2]));
                line = br.readLine ();
            }
        }
        catch (Exception e)
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
    }
    public static double quote(String A, String B){
        System.out.println(cache.get (A+B));
        if(cache.get (A+B)!=null){
        return cache.get (A+B);
        }
        else{
            if(cache.get (B+A)!=null){
                return 1/cache.get (B+A);
            }
            else{
                if(cache.get (A+"USD")!=null&& cache.get ("USD"+B)!=null){
                    return cache.get (A+"USD")*cache.get ("USD"+B);
                }
                else{
                    throw new IllegalArgumentException("Ticker is not valid!");
                }
            }
        }
    }
}

