package testart;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;



public class getFXQuoteTest
{
    getFXQuote val = new getFXQuote("111333");

    @Test
    public void USDCNY() 
        throws Exception
    {    
                
        assertEquals(6.6393, val.quote("CAD","CNY"),0.0001);
    }
    @Test
    public void USDCAD() 
        throws Exception
    {    
                
        assertEquals(0.9925, val.quote("USD","CAD"),0.0001);
    }
    @Test
    public void DJFUSD() 
        throws Exception
    {    
                
        assertEquals(0.005794, val.quote("DJF","USD"),0.0001);
    }
    @Test(expected = IllegalArgumentException.class)
    public void USDUSD() 
        throws Exception
    {    
                
        assertEquals(-1, val.quote("USD","USD"),0.0001);
    }
    @Test
    public void CADUSD() 
        throws Exception
    {    
                
        assertEquals(1.0069, val.quote("CAD","USD"),0.0001);
    }
    @Test
    public void TWDAMD() 
        throws Exception
    {    
                
        assertEquals(12.53868, val.quote("TWD","AMD"),0.0001);
    }
    @Test(expected = IllegalArgumentException.class)
    public void QARCNY() 
        throws Exception
    {    
                
        assertEquals(12.53868, val.quote("QAR","CNY"),0.0001);
    }
    @Test
    public void Constructor()
        throws Exception
        {
        assertEquals("111333",val.filepath);

        }
}
